# COVID19 modelling for two patches

## Installation
Install dependencies using _requirements.txt_ via command 
> ``pip install -r requirements.txt``

## Google Colab
For testing functionalities, there is a Jupyter notebook on Google Colaboratory 
(see [link](https://colab.research.google.com/drive/1L6fbvu15RR-AFNvV7kc0Kt8BuxkfRnCX?usp=sharing))
which can be used, if someone has access to this repository.

## Data
All public data can be found in the folder named _data_, aggregated, generated data can be found in data/generated
subfolder (after running the corresponding scripts).
- Files _dpc-covid19-ita-province.csv_ and _dpc-covid19-ita-regioni.csv_ were downloaded on 16th June from
[GitHub](https://github.com/pcm-dpc/COVID-19) (commit: `2e41d15cbc0dce3113b7f3274f3b2479b5d2487b`)
- Files _pop_per_prov.csv_ and _pop_per_reg.csv_ contains data from
[citypopulation.de](http://citypopulation.de/en/italy/admin/)
- Table _2020-6-15全国新冠疫情数据统计表.xlsx_ was provided by Maoxing Liu (North University of China, Department of
Mathematics, [ResearchGate](https://www.researchgate.net/profile/Maoxing_Liu)) and contains data collected from
[https://news.qq.com/zt2020/page/feiyan.htm?from=timeline#/global](https://news.qq.com/zt2020/page/feiyan.htm?from=timeline#/global)
- Since the previous excel file contains everything except the nationwide severe case counts, the daily situation
reports from NHC were downloaded and stored in the folder _nhc_data_.
- Jupyter notebook _chinese-data_translation-and-cleaning.ipynb_ translates and organize the raw chinese data into two
CSV files (_complete_china_data.csv_ and _complete_hubei_data.csv_) in subdirectory _data/generated_
- Running _source/dataloader.py_ generates smoothed, aggregated two-patch data that is used by the simulations.