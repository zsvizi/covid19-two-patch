import json
import os

import numpy as np
from lmfit import Parameters

import source
from source import parameters as p


class ParameterHandler:
    """
    Class for handling parameters during piecewise optimization
    Attributes
    ----------
    time_intervals: list
        contains time intervals associated to optimization steps
    parameters: list
        contains optimizer outcomes of all optimization steps
    init_values: list
        contains fitted initial values, if there is any for an optimization step
    phi: float
        fitted value for phi

    Methods
    -------
    add_time_interval(time_point)
        appends time interval to time_intervals list
    add_parameters(parameters)
        appends parameters dictionary to parameters list
    add_init_values(params_obj)
        append initial values, for which vary=True to init_values list
    update_initial_values(init_values)
        updates input array w.r.t the stored values in ParameterHandler instance
    save(file_path)
        saves stored data to a JSON file
    load(file_path)
        loads data from a JSON file and stores in the attributes
    """
    def __init__(self):
        self.time_intervals = []
        self.parameters = []
        self.init_values = []
        self.phi = None

    def add_time_interval(self, time_point: list) -> None:
        """
        Appends time interval to the time_intervals attribute
        :param time_point: list time interval to append
        :return: None
        """
        self.time_intervals.append(time_point)

    def add_parameters(self, parameters: dict) -> None:
        """
        Appends parameters dictionary to the parameters attribute
        :param parameters: dict derived dictionary from Parameters object resulting by the optimizer
        :return: None
        """
        self.parameters.append(parameters)

    def add_init_values(self, params_obj: Parameters) -> None:
        """
        Append initial values, which were fitted to the init_values attribute
        :param params_obj: Parameters result of the optimization
        :return: None
        """
        init_values = dict()
        for key in p.init_fitting:
            if params_obj[key].vary:
                init_values.update({key: params_obj[key].value})
        self.init_values.append(init_values)

    def update_initial_values(self, initial_values: np.ndarray) -> None:
        """
        Update input initial values array w.r.t. the stored output values of the optimization
        :param initial_values: np.ndarray input array containing initial values
        :return: None
        """
        if len(list(self.init_values[0].keys())) > 0:

            fitted_init_idx = np.array(
                [p.all_states[key]
                 for key in self.init_values[0].keys()
                 ]).flatten()

            fitted_init_values = np.array(
                [value
                 for value in self.init_values[0].values()
                 ]).flatten()

            initial_values[fitted_init_idx] = fitted_init_values

            # Compartments excluded from update
            excl_for_population_update = ["s", "c"]
            # Indices of included states in population update
            idx_update = np.array([p.all_states[state]
                                   for state in p.all_states.keys()
                                   if state.split('_')[0] not in excl_for_population_update
                                   ])

            update_idx = slice(p.n_patch * p.c_dict["s"], p.n_patch * (p.c_dict["s"] + 1))
            initial_values[update_idx] = \
                p.population - np.sum(initial_values[idx_update].reshape((-1, len(p.patches))),
                                      axis=0).flatten()

    def save(self, file_path: str = str(os.path.join(source.PATH, "data/generated/parameters.json"))) -> None:
        """
        Save internal data to a JSON file
        :param file_path: str path to the file to save
        :return: None
        """
        output = []
        for idx in range(len(self.time_intervals)):
            dictionary = {"time_interval": self.time_intervals[idx]}
            param_dict = {key: list(value)
                          for key, value in zip(self.parameters[idx].keys(), self.parameters[idx].values())
                          if key != "phi"}
            dictionary.update({"parameters": param_dict})
            if len(self.init_values) > idx:
                dictionary.update({"initial_values": self.init_values[idx]})
            output.append(dictionary)
        output.append({"phi": self.phi})
        with open(file_path, 'w') as f:
            json.dump(output, f, sort_keys=True, indent=4)

    def load(self, file_path: str = str(os.path.join(source.PATH, "data/generated/parameters.json"))) -> None:
        """
        Loads data from a JSON file and stores in the attributes
        :param file_path: str path to the file to load
        :return: None
        """
        with open(file_path, 'r') as f:
            parameters = json.load(f)
        self.time_intervals = [element["time_interval"] for element in parameters[:-1]]
        self.parameters = [{key: np.array(value)
                            for key, value in zip(element["parameters"].keys(), element["parameters"].values())}
                           for element in parameters[:-1]]
        self.init_values = [element["initial_values"] for element in parameters[:-1]]
        self.phi = parameters[-1]["phi"]

    def specify(self, no_time_intervals: int) -> "ParameterHandler":
        """
        Returns with a reduced ParameterHandler object
        :param no_time_intervals: int number of time intervals, which are considered
        :return: ParameterHandler reduced parameter handler object
        """
        param_hand = None
        if no_time_intervals <= len(self.time_intervals):
            param_hand = ParameterHandler()
            param_hand.time_intervals = self.time_intervals[:no_time_intervals]
            param_hand.init_values = self.init_values[:no_time_intervals]
            param_hand.parameters = self.parameters[:no_time_intervals]
            param_hand.phi = self.phi
        return param_hand
