from SALib.sample import latin, morris as smorris, saltelli


class Sampler:
    """
    A class to apply a sampling method

    Attributes
    ----------
    description : dict
        a dictionary that contains the names and numbers of variables and their sample bounds.
        description = {
            'num_vars': 3,
            'names': ['x1', 'x2', 'x3'],
            'bounds': [[-3.14159265359, 3.14159265359],
                       [-3.14159265359, 3.14159265359],
                       [-3.14159265359, 3.14159265359]]
        }
    number_of_samples : int
        number of samples (default 1000)
    output : ndarray
        each row of the output corresponds to a sample
    """

    def __init__(self, description: dict, method: str = "LHS", number_of_samples: int = 1000):
        """
        Initialize a Sampler object

        :param description: a dictionary that define the sampling variables and their bounds
        :param method: method of the sampling method: "LHS", "saltelli" or "morris"
        :param number_of_samples: number of desired samples (default is 1000)
        """
        self.description = description
        self.number_of_samples = number_of_samples
        self.output = None
        if method == "LHS":
            self.__create_lhs_sample()
        elif method == "saltelli":
            self.__create_saltelli_sample()
        elif method == "morris":
            self.__create_morris_sample()

    def __create_lhs_sample(self) -> None:
        """
        Create samples using latin hypercube sampling method
        :return None:
        """
        self.output = latin.sample(self.description, self.number_of_samples)

    def __create_saltelli_sample(self) -> None:
        """
        Create samples using Saltelli's method
        :return None:
        """
        self.output = saltelli.sample(self.description, self.number_of_samples)

    def __create_morris_sample(self) -> None:
        """
        Create samples using Morris method
        :return None:
        """
        self.output = smorris.sample(self.description, self.number_of_samples)