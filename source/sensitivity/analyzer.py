import os

from matplotlib import cm, pyplot as plt
import numpy as np
from SALib.analyze import morris as amorris, sobol

import source


class Analyzer:
    """
    A class to run a sensitivity analysis

    Attributes
    ----------
    input_table : ndarray
        each row corresponds to a sample
    target : ndarray
        each element corresponds to the appropriate row of the input_table
    variables : list
        an ordered list with the name of variables
    method : str
        defines method of the analysis
    """

    def __init__(self, input_table: np.ndarray, target: np.ndarray, variables: list, method: str):
        self.input = input_table
        self.target = target
        self.variables = variables
        self.output = None
        if method == "morris":
            self.__calculate_morris_indices()
        elif method == "sobol":
            self.__calculate_sobol_indices()
        elif method == "prcc":
            self.__calculate_prcc_indices()

    def __calculate_sobol_indices(self) -> None:
        """
        Calculate sobol sensitivity indices
        IMPORTANT: sobol uses saltelli sampling

        :return: None
        """
        settings = {
            'num_vars': len(self.variables),
            'names': self.variables,
        }
        analysis_result = sobol.analyze(settings, self.target, print_to_console=False)
        self.output = analysis_result['S1']

    def __calculate_morris_indices(self) -> None:  # Requires morris sample method
        """
        Calculate morris sensitivity indices
        IMPORTANT: morris uses morris sampling

        :return None:
        """
        settings = {
            'num_vars': len(self.variables),
            'names': self.variables,
        }
        analysis_result = amorris.analyze(settings, self.input, self.target, print_to_console=False)
        self.output = analysis_result['mu_star']

    def __calculate_prcc_indices(self) -> None:
        """
        Calculate PRCC sensitivity indices
        IMPORTANT: PRCC uses LHS sampling

        :return None:
        """
        lhs_output_table = np.c_[self.input, self.target]
        ranked = (lhs_output_table.argsort(0)).argsort(0)
        corr_mtx = np.corrcoef(ranked.T)
        if np.linalg.det(corr_mtx) < 1e-50:  # determine if singular
            corr_mtx_inverse = np.linalg.pinv(corr_mtx)  # may need to use pseudo inverse
        else:
            corr_mtx_inverse = np.linalg.inv(corr_mtx)

        parameter_count = lhs_output_table.shape[1] - 1

        prcc_vector = np.zeros(parameter_count)
        for w in range(parameter_count):  # compute PRCC btwn each param & sim result
            prcc_vector[w] = -corr_mtx_inverse[w, parameter_count] / \
                             np.sqrt(corr_mtx_inverse[w, w] *
                                     corr_mtx_inverse[parameter_count, parameter_count])
        self.output = prcc_vector

    def plot_sensitivity_indices(self, file_name: str) -> None:
        """
        Plots sensitivity index calculated for this Analyzer instance
        :return: None
        """

        plt.rc('xtick', labelsize=14)
        plt.rc('ytick', labelsize=14)
        plt.rc('font', size=14)

        parameter_count = len(self.variables)
        xp = range(parameter_count)
        plt.figure(figsize=(6, 4))
        plt.margins(0, tight=False)
        plt.tick_params(direction="in")
        # colors = ['firebrick', 'green', 'blue', 'black', 'red', 'purple', 'skyblue']
        colors = cm.summer(np.array(self.output) / (2 * float(max(np.array(self.output)))))
        plt.bar(xp, list(self.output), align='center', color=colors, zorder=2)
        plt.ylim(min(self.output) - 0.05, max(self.output) + 0.05)
        var_label = []
        for var in self.variables:
            var_label.append(r'{}'.format("$\\" + var + "$"))
        plt.xticks(ticks=xp, labels=var_label)
        plt.tight_layout()
        plt.grid(axis='y')
        ax = plt.gca()
        plt.savefig(os.path.join(source.PATH, 'data/generated', file_name + '.eps'), format='eps')
        plt.show()
