from typing import Tuple

import numpy as np
import pandas as pd

from source.model import TwoPatchModel
from source.parameter_handler import ParameterHandler
from source.sensitivity.sampler import Sampler
from source.utils import convert_to_parameter_dict, get_parameter_df
from source import parameters as p


class TargetGenerator:
    """
    Class for generating target values for Analyzer from input Sampler object
    Attributes
    ----------
    sample_extended: pd.DataFrame
        contains all parameters needed for the calculations via extending sampler output by the not varied parameters

    Methods
    -------
    test_relation()
        simple algebraic function to test sensitivity analysis features
    generate_r0_values(patch)
        wrapper around R0 calculation in the model
    generate_final_size_values(init, patch, param_hand)
        wrapper around final size calculation in the model
    generate_outbreak_peak_values(init_values, patch)
        wrapper around outbreak peak calculation in the model
    """
    def __init__(self, sampler: Sampler, param_hand: ParameterHandler, varied_parameters: list):
        self.sample_extended = None
        self.param_hand = param_hand
        self.__get_extended_sample(param_hand=param_hand,
                                   varied_parameters=varied_parameters, sampler=sampler)

    def test_relation(self) -> np.ndarray:
        """
        Simple algebraic function to test SA features
        :return: np.ndarray computed target array
        """
        return 8 ** self.sample_extended[:, 0] + 5 * self.sample_extended[:, 1] + self.sample_extended[:, 2]

    def generate_r0_values(self, patch: str) -> np.ndarray:
        """
        Wrapper method around R0 calculation
        :param patch: str specifies which patch is considered in the sensitivity analysis
        :return: np.ndarray computed target array
        """
        param_df = get_parameter_df(sample_extended=self.sample_extended, patch=patch)
        model = TwoPatchModel
        r0_values = np.array(model.get_r0(ps=param_df))
        return r0_values

    def generate_final_size_values(self, init_values: np.ndarray, patch: str,
                                   param_hand: ParameterHandler) -> np.ndarray:
        """
        Wrapper method around final size calculation
        :param init_values: np.ndarray initial values for solving the model
        :param patch: str specifies which patch is considered in the sensitivity analysis
        :param param_hand: ParameterHandler contains parameters from the optimization
        :return: np.ndarray computed target array
        """
        final_size = []
        for param_vector in self.sample_extended.values:
            # Get dictionary of parameter values containing keys with patches in their names
            params = {parameter: {"value": value}
                      for parameter, value in zip(self.sample_extended.columns, param_vector)}

            # Convert params to the standard parameter dictionary
            epi_param = convert_to_parameter_dict(params)

            # Specify ParameterHandler to the first time interval
            spec_param_hand = param_hand.specify(no_time_intervals=1)

            # Change time of lock-down
            if "T" in list(epi_param.keys()):
                p.control_parameters["T"]["value"] = int(epi_param["T"][0])
                spec_param_hand.time_intervals[0][1] = list(p.dates.keys())[int(epi_param["T"][0])]

            # Get piecewise parametrized solution for initial value to the final size calculation
            model = TwoPatchModel(final_size=True)
            _, solution = model.get_piecewise_parametrized_solution(param_hand=spec_param_hand,
                                                                    init_values=init_values)

            # Initial value for final size calculation
            init = solution[-1, :]

            # Calculate analytical final size and filter for the specified patch
            final_size.append(model.get_analytical_final_size(epi_param=epi_param,
                                                              initial_values=init
                                                              )[p.patches.index(patch)]
                              )
        final_size_values = np.array(final_size)
        return final_size_values

    def generate_outbreak_peak_values(self, init_values: np.ndarray, patch: str) -> np.ndarray:
        """
        Wrapper around outbreak peak calculation for the model
        :param init_values: np.ndarray contains initial values for the solver
        :param patch: str specified patch for which the peak is calculated
        :return: np.ndarray calculated target values
        """
        model = TwoPatchModel()
        outbreak_peak = []
        for param_vector in self.sample_extended.values:
            # Get dictionary of parameter values containing keys with patches in their names
            params = {parameter: {"value": value}
                      for parameter, value in zip(self.sample_extended.columns, param_vector)}

            # Convert params to the standard parameter dictionary
            epi_param = convert_to_parameter_dict(params)

            if 'rho' in epi_param.keys():
                rho_A, rho_B = epi_param["rho"]
                # Update E(0) to match the detection rate
                e_idx = np.array([idx for idx, state in enumerate(p.all_states.keys()) if state in ['e_A', 'e_B']])
                init_values[e_idx] = np.array([(1606-226)/rho_A, (88-3)/rho_B]).round()
                # Update U(0) to match the detection rate
                i_A, i_B = p.init['i']
                if p.init['i'][1] == 0:
                    i_B += 1
                u_idx = np.array([idx for idx, state in enumerate(p.all_states.keys()) if state in ['u_A', 'u_B']])
                init_values[u_idx] = np.array([(1-rho_A)*i_A/rho_A, (1-rho_B)*i_B/rho_B]).round()
                # Recalculate S(0)
                excl_for_population_update = ["s", "c"]
                idx_update = np.array([p.all_states[state]
                                       for state in p.all_states.keys()
                                       if state.split('_')[0] not in excl_for_population_update
                                       ])
                update_idx = np.array([idx for idx, state in enumerate(p.all_states.keys()) if state in ['s_A', 's_B']])
                init_values[update_idx] = p.population - \
                    np.sum(init_values[idx_update].reshape((-1, len(p.patches))), axis=0).flatten()

            outbreak_peak.append(
                model.get_outbreak_peak(init_values=init_values,
                                        epi_param=epi_param,
                                        patch=patch)
            )
        outbreak_peak_values = np.array(outbreak_peak)
        return outbreak_peak_values

    def generate_model_solutions(self, init_values: np.ndarray) -> Tuple[np.ndarray, list]:
        model = TwoPatchModel()
        start = p.dates["2020-02-24"]
        end = p.dates["2020-03-15"]
        time_vector = np.linspace(start, end, int(end - start) * p.day_step + 1)[:-1]

        model_solutions = []
        for param_vector in self.sample_extended.values:
            params = {parameter: {"value": value}
                      for parameter, value in zip(self.sample_extended.columns, param_vector)}

            epi_param = convert_to_parameter_dict(params)
            model_solutions.append(model.get_solution(t=time_vector, epi_param=epi_param, init_values=init_values))

        return time_vector, model_solutions

    def __get_extended_sample(self, param_hand: ParameterHandler,
                              sampler: Sampler,
                              varied_parameters: list) -> None:
        """
        Extends output of the sampler by the values of the not varied parameters
        :param param_hand: ParameterHandler contains fitting output
        :param varied_parameters: list contains varied parameters (full name extended by the patch)
        :return: None
        """
        # Get parameter names and values as two lists from the param_hand
        param_names = []
        values = []
        for key in param_hand.parameters[0].keys():
            param_names.extend([key + "_" + patch for patch in p.patches])
            if len(param_hand.parameters[0][key]) == len(p.patches):
                values.extend(list(param_hand.parameters[0][key]))
            else:
                values.extend(list(np.repeat(param_hand.parameters[0][key], repeats=len(p.patches))))
        # Append T and phi and their values
        param_names.append("T")
        values.append(p.control_parameters["T"]["value"])
        param_names.append("phi")
        values.append(param_hand.phi)

        # Create base dictionary for the extended sample DataFrame
        df_base = {param: list(value * np.ones(sampler.output.shape[0]))
                   for value, param in zip(values, param_names)}

        # Create extended samples
        extended_sample_df = pd.DataFrame(df_base)

        # Change values at columns of the varied parameters
        extended_sample_df[varied_parameters] = sampler.output

        # Vary values of betaI and betaH to remain as functions of betaU
        for patch in p.patches:
            f_i = p.epidemic_parameters['f_i_'+patch]['value']
            f_h = p.epidemic_parameters['f_h_'+patch]['value']
            extended_sample_df['beta_i_'+patch] = f_i * extended_sample_df['beta_u_'+patch]
            extended_sample_df['beta_h_'+patch] = f_h * extended_sample_df['beta_u_'+patch]

        self.sample_extended = extended_sample_df
