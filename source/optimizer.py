import copy

from lmfit import Minimizer, Parameters
from lmfit.minimizer import MinimizerResult
import numpy as np

from source import parameters as p
from source.model import TwoPatchModel
from source.utils import convert_to_parameter_dict, get_parameters_from_param_dict


class Optimizer:
    """
    Class for fitting process

    Attributes
    ----------
    time_vector: np.ndarray
        contains time points associated to the data
    init_values: np.ndarray
        contains initial values for the model solver
    data: np.ndarray
        contains data used for fitting parameters
    model: TwoPatchModel
        model object used in this project
    last_day_cumulative: np.ndarray
        contains cumulative values for incidence calculation
    filters: dict
        contains the following filters used in the residual calculation
            - time: filter for daily state values of the solution vector
            - state: filter for specific states of the model which are used in the fitting process
            - init: filter for initial values which are estimated in the fitting process
    params: Parameters
        contains parameters of the model in a Parameters object, which is usable in the fitting process
    minimizer: Minimizer
        object for optimization during fitting process
    result: MinimizerResult
        contains result of the fitting process
    """
    def __init__(self, time_vector: np.ndarray, data: np.ndarray, init_values: np.ndarray, params: Parameters,
                 last_day_cumulative: np.ndarray, model: TwoPatchModel):
        # Time vector
        self.time_vector = time_vector

        # Initial values
        self.init_values = init_values

        # Cumulative values for calculating incidence for the starting day
        self.last_day_cumulative = last_day_cumulative

        # Data to fit
        self.data = data

        # Parameters of the fitting
        self.params = params

        # Instantiate the model
        self.model = copy.deepcopy(model)
        self.population_after_migration = []
        if (self.model.population != p.population).all():
            self.population_after_migration = copy.deepcopy(self.model.population)

        # Preliminary calculations: filters for model solution
        self.filters = None
        self.__prepare_filters()

        # Fit the model to the given data
        self.minimizer = None
        self.result = None
        self.__fit_model()

    def __prepare_filters(self) -> None:
        """
        Set up filters for fitting process (for more details, see documentation of this class)
        :return: None
        """
        # Create dictionary of filters
        self.filters = dict()

        # Since optimizer get a time interval and the model is solved only for this interval
        # slice should start from 0
        self.filters.update({"time": slice(0,
                                           (int(np.max(self.time_vector)) - int(np.min(self.time_vector))) * p.day_step,
                                           p.day_step)})

        # Filter for states with corresponding data
        # note, that sorted() is important for consistent association of compartment state and data
        filter_idx = [p.all_states[state] for state in sorted(p.fitting)]
        self.filters.update({"state": np.array(filter_idx)})

        # Filter for initial values, which are variables in the fitting process
        # note, that sorted() is important for consistent association of compartment state and data
        self.filters.update({"init": np.array([p.all_states[state]
                                               for state in sorted(list(p.init_fitting.keys()))
                                               if self.params[state].vary
                                               ])
                             })

    def __fit_model(self) -> None:
        """
        Executes fitting process containing the following steps:
          - creates Minimizer object used in the fitting process
          - executes parameter fitting
        :return: None
        """

        # Estimate parameters for the model based on the given data
        self.minimizer = Minimizer(userfcn=self.__get_residual,
                                   params=self.params,
                                   nan_policy='omit',
                                   fcn_args=(self.time_vector, self.data))

        self.result = self.minimizer.minimize(method='lbfgsb')

    def __get_residual(self, ps: Parameters, ts: np.ndarray, data: np.ndarray) -> np.ndarray:
        """
        Calculates residual for optimization of fitting process
        :param ps: Parameters contains current parameters of the model
        :param ts: np.ndarray time vector for the model solver
        :param data: np.ndarray contains data used for the parameter estimation
        :return: np.ndarray residual calculated for the current step of fitting process
        """
        # Update initial values, if it is necessary
        self.__update_initial_values(ps=ps)

        # Get parameters
        param = convert_to_parameter_dict(ps=ps)

        # Get solution for error calculation
        self.model.population = np.array(p.population).astype(np.float64)
        solution = self.model.get_solution(t=ts,
                                           epi_param=param,
                                           init_values=self.init_values,
                                           last_day_cumulative=self.last_day_cumulative,
                                           population_after_migration=self.population_after_migration)

        # For fitting of dead individuals, we use d + du
        du_idx = [p.all_states[x] for x in p.all_states.keys() if x.split("_")[0] in ["du"]]
        d_idx = [p.all_states[x] for x in p.all_states.keys() if x.split("_")[0] in ["d"]]
        solution[:, d_idx] += solution[:, du_idx]

        # Calculate residual
        residual = (solution[self.filters["time"]][:, self.filters["state"]] - data).flatten()

        return residual

    def __update_initial_values(self, ps: Parameters) -> None:
        """
        Updates initial values before solving the model
        :param ps: Parameters contains current values of the parameters
        :return: None
        """
        if len(self.filters["init"]) > 0:
            # Update initial values used in fitting
            self.init_values[self.filters["init"]] = \
                np.array([ps[str(key)].value
                          for key in sorted(list(p.init_fitting.keys()))
                          if self.params[key].vary
                          ])

            # Compartments excluded from update
            excl_for_population_update = ["s", "c"]
            # Indices of included states in population update
            idx_update = np.array([p.all_states[state]
                                   for state in p.all_states.keys()
                                   if state.split('_')[0] not in excl_for_population_update
                                   ])
            # Update populations
            update_idx = slice(p.n_patch * p.c_dict["s"], p.n_patch * (p.c_dict["s"] + 1))
            self.init_values[update_idx] = p.population - \
                np.sum(self.init_values[idx_update].reshape((-1, len(p.patches))), axis=0).flatten()


def setup_parameters(list_for_vary: list, list_for_vary_control: list, list_for_vary_init: list,
                     optimizer: Optimizer, t_iv: int) -> Parameters:
    """
    Set up parameters for a step in piecewise optimization
    :param list_for_vary: list variable parameters in the current step
    :param list_for_vary_control: list variable control parameters in the current step
    :param list_for_vary_init: list variable initial values in the current step
    :param optimizer: Optimizer parametrized Optimizer object from the previous step
    :param t_iv: int index of current optimization steps
    :return: Parameters prepared Parameters object
    """

    # Check whether the first time period is considered
    if t_iv > 0:
        # Get parametrization from the previous time period
        params = optimizer.result.params
        # Change variability: True -> False
        for key in params.keys():
            if params[key].vary:
                params[key].vary = False

            # set the upper bound for betas as the beta from the previous time interval
            # if 'beta' in key and t_iv in [len(list_for_vary)-1, len(list_for_vary)-2]:
            #    params[key].min = params[key].value
    else:
        # Get parameters dictionary from parameters.py for the first period
        params = get_parameters_from_param_dict()

    # Set vary option for active parameters in the next optimization step
    for key in list_for_vary[t_iv]:
        params[key].vary = True

    # Set vary option for active control parameters in the next optimization step
    if len(list_for_vary_control[t_iv]) > 0:
        for key in list_for_vary_control[t_iv]:
            params[key].vary = True

    # Set vary option for active initial values in the next optimization step
    if len(list_for_vary_init[t_iv]) > 0:
        for key in list_for_vary_init[t_iv]:
            params[key].vary = True

    return params
