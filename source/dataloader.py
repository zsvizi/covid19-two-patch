import os
import typing

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import source
from source import parameters as p


class Dataloader:

    def __init__(self, country="Italy", patch_version=1):

        # Initial patch settings. To fulfill these data it is required to run one of the load_patch_data_X function
        # after creating a Dataloader object
        self.patch_data = None
        self.population = {"A": 0, "B": 0}
        self.country = country

        if self.country == "Italy":
            # Load regional data of Italy
            self._data_reg = pd.read_csv(os.path.join(source.PATH, 'data/dpc-covid19-ita-regioni.csv'))

            # Load provincial data of Italy
            self._data_prov = pd.read_csv(os.path.join(source.PATH, 'data/dpc-covid19-ita-province.csv'))

            # Load population data
            self._region_pop_data = pd.read_csv(os.path.join(source.PATH, 'data/pop_per_reg.csv'))
            self._province_pop_data = pd.read_csv(os.path.join(source.PATH, 'data/pop_per_prov.csv'))

            self.__load_italian_patch_data(patch_version)

        elif self.country == "China":
            self.population = {"A": 58160000, "B": 1439207085 - 58160000}  # Hupej - China (excluding Hupej)
            self.__load_chinese_patch_data()

    def __load_chinese_patch_data(self) -> None:
        """
        Loads data for a two-patch model. It fulfills the patch_data and population parameter of the dataloader
            - Patch A is Hubei
            - Patch B is the rest of China

        :return: None
        """
        data_hubei = pd.read_csv(os.path.join(source.PATH, 'data/generated/complete_hubei_data.csv'),
                                 index_col='date')
        data_china = pd.read_csv(os.path.join(source.PATH, 'data/generated/complete_china_data.csv'),
                                 index_col='date')
        data_hubei['current_severe'] = data_hubei['current_severe'].add(data_hubei['current_critically_ill'],
                                                                        fill_value=0)
        columns = ['daily_inf', 'total_recovered', 'total_deaths', 'current_severe']
        data_a = data_hubei[columns]
        data_c = data_china[columns]
        data_b = data_c - data_a

        merged_data = pd.DataFrame()
        for c in columns:
            merged_data[c, 'A'] = data_a[c]
            merged_data[c, 'B'] = data_b[c]
            merged_data[c, 'C'] = data_c[c]
        self.patch_data = merged_data

    def __load_italian_patch_data(self, patch_version: int = 1) -> None:
        """
        Loads data for a two-patch model. It fulfills the patch_data and population parameter of the dataloader

        :param patch_version: select the version of separation for patches (default 1)
            - 1: Patch A is the set of locked down regions: 'Lombardia', 'Emilia-Romagna', 'Piemonte', 'Veneto',
            'Marche'; Patch B is the rest of Italy
            - 2: Patch A is the set of locked down provinces: 'Alessandria', 'Asti', 'Bergamo', 'Brescia', 'Como',
            'Cremona', 'Lecco', 'Lodi', 'Mantua', 'Milan', 'Modena', 'Monza e della Brianza', 'Novara', 'Padua',
            'Parma', 'Pavia', 'Pesaro e Urbino', 'Piacenza', 'Reggio nell\'Emilia', 'Rimini', 'Sondrio', 'Treviso',
            'Varese', 'Venice', 'Verbano-Cusio-Ossola', 'Vercelli'; Patch B is the rest of Italy
        :return: None
        """

        if patch_version == 1:
            region_list = ['Lombardia', 'Emilia-Romagna', 'Piemonte', 'Veneto', 'Marche']
            data_a = self._data_reg.loc[self._data_reg['denominazione_regione'].isin(region_list)]
            data_b = self._data_reg.loc[~self._data_reg['denominazione_regione'].isin(region_list)]
            columns = ['totale_ospedalizzati', 'nuovi_positivi', 'dimessi_guariti', 'deceduti', 'totale_casi',
                       'tamponi', 'casi_testati', 'terapia_intensiva']
        elif patch_version == 2:
            region_list = ['Alessandria', 'Asti', 'Bergamo', 'Brescia', 'Como', 'Cremona', 'Lecco', 'Lodi', 'Mantua',
                           'Milan', 'Modena', 'Monza e della Brianza', 'Novara', 'Padua', 'Parma', 'Pavia',
                           'Pesaro e Urbino', 'Piacenza', 'Reggio nell\'Emilia', 'Rimini', 'Sondrio', 'Treviso',
                           'Varese', 'Venice', 'Verbano-Cusio-Ossola', 'Vercelli']
            data_a = self._data_prov.loc[self._data_prov['denominazione_provincia'].isin(region_list)]
            data_b = self._data_prov.loc[~self._data_prov['denominazione_provincia'].isin(region_list)]
            columns = ['totale_casi']
        else:
            raise Exception("There is no given patch version for this country.")

        data_a = data_a.groupby(['data']).sum()  # Aggregated for patch A
        data_b = data_b.groupby(['data']).sum()  # Aggregated for patch B

        data_a = data_a[columns]
        data_b = data_b[columns]

        merged_data = pd.DataFrame()
        for c in columns:
            merged_data[c, 'A'] = data_a[c]
            merged_data[c, 'B'] = data_b[c]

        dates = merged_data.index.to_list()
        for i in range(len(dates)):
            dates[i] = dates[i][:10]
        merged_data.index = dates

        self.patch_data = merged_data

        pop_data_A = self._region_pop_data.loc[self._region_pop_data['Territory'].isin(region_list)]

        self.population["A"] = pop_data_A["Value"].sum()
        self.population["B"] = self._region_pop_data["Value"].sum() - self.population["A"]


def init_from_data(data_loader: Dataloader) -> None:
    """
    Initialize parameters from data
    :param data_loader: DataLoader, loaded data in a DataLoader object
    :return: None
    """
    # Get population data
    p.population = list(data_loader.population.values())
    # Initial values for S compartments
    p.init.update({'s': [p.population[0] - sum([p.init[comp][0] for comp in p.init.keys() if 'c' not in comp]),
                         p.population[1] - sum([p.init[comp][1] for comp in p.init.keys() if 'c' not in comp])
                         ]})

    # Get dates from data files
    p.dates.update({date: idx
                    for idx, date in enumerate([row_idx.split('T')[0]
                                                for row_idx in list(data_loader.patch_data.index)])
                    })


def get_data(data_loader: Dataloader, start_day: str, end_day: str,
             columns: typing.List[str]) -> typing.Tuple[np.ndarray, np.ndarray]:
    """
    Interface getter for data
    :param columns: typing.List[str] filtered columns
    :param data_loader: DataLoader loaded data
    :param start_day: str starting date time
    :param end_day: str end date time
    :return: typing.Tuple[np.ndarray, np.ndarray] data and time vector derived from the data
    """
    # Load data
    if data_loader.country == 'Italy':
        comp_to_data = p.comp_to_data_it
    else:
        comp_to_data = p.comp_to_data_chn
    # Filter for fitting data in data loader
    data = np.array([data_loader.patch_data[comp_to_data[comp]] for comp in sorted(columns)]).T

    # Get time vector for fitting
    if end_day is None:
        data_time_vector = np.linspace(0, data.shape[0], data.shape[0] * p.day_step + 1)
    else:
        data_time_vector = np.linspace(p.dates[start_day], p.dates[end_day],
                                       (p.dates[end_day] - p.dates[start_day]) * p.day_step + 1)
        data = data[int(p.dates[start_day]):int(p.dates[end_day])]
    return data, data_time_vector


def generate_chinese_data() -> None:
    """
    Runs IPYNB to generate Chinese data from raw data files
    :return: None
    """
    if not os.path.exists(os.path.join(source.PATH, 'data/generated')):
        os.mkdir(os.path.join(source.PATH, 'data/generated'))
        os.system('runipy ' + str(os.path.join(source.PATH, 'data', 'chinese-data_translation-and-cleaning.ipynb')))


def generate_plots(df: pd.DataFrame, y_list: list, file_name: str) -> None:
    plt.grid(linestyle=':', linewidth=0.5)
    plot_list = []
    for y in y_list:
        pi, = plt.plot(df.index, df[y], label=y)
        plot_list.append(pi)
    plt.legend(handles=plot_list)
    plt.xticks(rotation='vertical')
    plt.xlabel("Date")
    plt.ylabel("")
    ax = plt.gca()
    ax.set_xticks(ax.get_xticks()[::5])
    fig = plt.gcf()
    fig.set_size_inches(8, 4)
    plt.savefig(os.path.join(source.PATH, 'data/generated', file_name), bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    dl = Dataloader(country="Italy", patch_version=1)
    dl.patch_data.to_excel(os.path.join(source.PATH, 'data/generated/output1.xls'))
    plots = [
        [[('totale_ospedalizzati', 'A'), ('totale_ospedalizzati', 'B')], "Italy_p1_H.pdf"],
        [[('nuovi_positivi', 'A'), ('nuovi_positivi', 'B')], "Italy_p1_I.pdf"],
        [[('deceduti', 'A'), ('deceduti', 'B')], "Italy_p1_D.pdf"]
    ]
    for y in plots:
        generate_plots(dl.patch_data, y[0], file_name=y[1])

    dl = Dataloader(country="Italy", patch_version=2)
    dl.patch_data.to_excel(os.path.join(source.PATH, 'data/generated/output2.xls'))

    dl = Dataloader(country="China")
    dl.patch_data.to_excel(os.path.join(source.PATH, 'data/generated/output3.xls'))
    plots = [
        [[('daily_inf', 'A'), ('daily_inf', 'B')], "China_I.pdf"],
        [[('total_recovered', 'A'), ('total_recovered', 'B')], "China_R.pdf"],
        [[('total_deaths', 'A'), ('total_deaths', 'B')], "China_D.pdf"],
        [[('current_severe', 'A'), ('current_severe', 'B')], "China_H.pdf"]
    ]
    for y in plots:
        generate_plots(dl.patch_data, y[0], file_name=y[1])
