import copy
from typing import Union, Tuple

import numpy as np
import pandas as pd
from scipy.integrate import odeint
from scipy.optimize import fsolve

import source.parameters as p
from source.parameter_handler import ParameterHandler
from source.utils import get_comp_idx


class TwoPatchModel:
    """
    Model implementation

    Attributes
    ----------
    population: np.ndarray
        contains population of patches

    Methods
    -------
    get_solution(t, epi_param_1, epi_param_2, init_values)
        Calculates solution of model for specified parameters
    get_final_size(t, epi_param_1, epi_param_2, init_values)
        Calculates final size for the model
    get_outbreak_peak(t, epi_param_1, epi_param_2, init_values)
        Calculates outbreak peak for the model
    get_r0(ps)
        Calculates model-specific R0
    """

    def __init__(self, final_size: bool = False):
        self.population = np.array(p.population).astype(np.float64)
        self.last_day_cumulative = None
        self.population_calculated_for_final_size = final_size

    def get_solution(self, t: np.ndarray, epi_param: dict, init_values: np.ndarray,
                     last_day_cumulative: np.ndarray = None,
                     population_after_migration: list = []) -> np.ndarray:
        """
        Interface function for plotting and fitting, returns solution array
        (states values for all time points and compartments)
        :param last_day_cumulative: np.ndarray input for initial incidence calculation on the starting day
        :param t: np.ndarray time vector for ODE solver
        :param epi_param: dict parameters of the model before time T
        :param init_values: np.ndarray initial values for the ODE solver
        :param population_after_migration: list population after migration, necessary during optimization
        :return: np.ndarray solution vector
        """

        # Initial population
        if population_after_migration != []:
            self.population = copy.deepcopy(population_after_migration)

        # Get initial value
        init = copy.deepcopy(init_values)

        # This function does not expect impulse inside the time interval defined by t
        # Impulse changes only the initial value of the model for the lock-down time interval
        if int(t[0]) == p.control_parameters["T"]["value"]:
            self.__execute_impulse(init=init, param=epi_param)

        # Calculate cumulative value and set in the initial values array
        c_excl_list = ["s", "e", "u", "ru", "du", "c"]
        c_sum_idx = [p.all_states[x] for x in p.all_states.keys() if x.split("_")[0] not in c_excl_list]
        init[p.c_idx] = np.sum(init[np.array(c_sum_idx).reshape((-1, p.n_patch))], axis=0)

        # Solve model
        solution = np.array(odeint(self.__get_model, init, t, args=(epi_param,)))

        self.last_day_cumulative = solution[-p.day_step:, p.c_idx]

        # Change c(t) to daily incidence
        if last_day_cumulative is None:
            first_day_cumulative = np.tile(init[p.c_idx], reps=(p.day_step, 1))
            daily_incidence = np.concatenate([first_day_cumulative,
                                              solution[p.day_step:2*p.day_step, p.c_idx] - first_day_cumulative,
                                              solution[2*p.day_step:, p.c_idx] - solution[p.day_step:-p.day_step,
                                                                                          p.c_idx]],
                                             axis=0)  # calculate incidence for all t values
        else:
            daily_incidence = np.append(init_values[p.c_idx].reshape((-1, 2)),
                                        solution[p.day_step:, p.c_idx] - solution[:-p.day_step, p.c_idx],
                                        axis=0)  # calculate incidence for all t values
        solution[:, p.c_idx] = daily_incidence
        if last_day_cumulative is None:
            solution[:p.day_step, p.c_idx] = np.tile(p.init["c"], reps=(p.day_step, 1))

        return solution

    def get_piecewise_parametrized_solution(self, param_hand: ParameterHandler,
                                            init_values: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        Solve model on time intervals located in input ParameterHandler object
        :param param_hand: ParameterHandler container for parametrization on all time intervals
        :param init_values: np.ndarray initial values for the model solver
        :return: Tuple[np.ndarray, np.ndarray] time vector and solution array
        """
        time_vectors = []
        for t_iv in param_hand.time_intervals:
            start = p.dates[t_iv[0]]
            end = p.dates[t_iv[1]]
            time_vectors.append(np.linspace(start, end, int(end - start) * p.day_step + 1))
        self.population = np.array(p.population).astype(np.float64)
        model_solution = self.get_solution(t=time_vectors[0],
                                           epi_param=param_hand.parameters[0],
                                           init_values=init_values)
        ts = copy.deepcopy(time_vectors[0])
        for idx in range(1, len(time_vectors)):
            sol = self.get_solution(t=time_vectors[idx],
                                    epi_param=param_hand.parameters[idx],
                                    init_values=model_solution[-1, :],
                                    last_day_cumulative=self.last_day_cumulative)
            model_solution = np.append(model_solution, sol[1:, :], axis=0)
            ts = np.append(ts, time_vectors[idx][1:], axis=0)
        return ts, model_solution

    def get_final_size(self, t: np.ndarray, epi_param: dict, init_values: np.ndarray) -> np.ndarray:
        """
        Calculates final sizes of all patches for input parametrization of the model
        :param t: np.ndarray time vector
        :param epi_param: dict parametrization of the model
        :param init_values: np.ndarray initial values
        :return: np.ndarray calculated final sizes
        """

        solution = self.get_solution(t=t, epi_param=epi_param, init_values=init_values)
        # Compartments for final size calculations
        final_comps = ['r', 'ru', 'd']
        final_idx = np.array(get_comp_idx(final_comps))
        final_size = np.sum(solution[-1, final_idx], axis=0)
        return final_size

    def get_analytical_final_size(self, epi_param: dict,
                                  initial_values: np.ndarray) -> np.ndarray:
        """
        Calculates analytical final size
        :param epi_param: dict parameter dictionary
        :param initial_values: np.ndarray initial values at time point of lockdown, before impulsive change
        :return: np.ndarray final size for the patches
        """

        def get_eqn(fs: np.ndarray, pop: np.ndarray, ps: dict, i_v: np.ndarray) -> np.ndarray:
            """
            Outputs lhs-rhs value from the system of equations
            :param fs: np.ndarray previous value of lhs-rhs
            :param pop: np.ndarray population of the patches
            :param ps: dict parameter dictionary
            :param i_v: np.ndarray initial values at time point of lockdown before impulse
            :return: np.ndarray evaluation of lhs-rhs of the system of the equations
            """
            s_idx = get_comp_idx(comp="s")
            r_idx = get_comp_idx(comp="r")
            ru_idx = get_comp_idx(comp="ru")
            h_idx = get_comp_idx(comp="h")
            d_idx = get_comp_idx(comp="d")
            du_idx = get_comp_idx(comp="du")
            e_idx = get_comp_idx(comp="e")
            u_idx = get_comp_idx(comp="u")

            c_1_num = ps["beta_h"] * ps["eta"] + ps["beta_i"] * (ps["gamma_h"] + ps["delta_h"])
            c_1_den = pop * (ps["gamma_h"] * (ps["gamma_i"] + ps["eta"]) + ps["gamma_i"] * ps["delta_h"])
            c_1 = c_1_num / c_1_den

            c_2_num = ps["beta_u"]
            c_2_den = pop * ps["gamma_u"] * (1 - ps["sigma"])
            c_2 = c_2_num / c_2_den

            c_3_num = ps["beta_i"] * ps["gamma_h"] - ps["beta_h"] * ps["gamma_i"]
            c_3_den = pop * (ps["gamma_i"] * ps["delta_h"] + ps["gamma_h"] * (ps["gamma_i"] + ps["eta"]))
            c_3 = c_3_num / c_3_den

            xi_3_num = ps["beta_u"]
            xi_3_den = pop * ps["gamma_u"] * ps["sigma"]
            xi_3 = xi_3_num / xi_3_den

            xi_2_num_1 = ps["eta"] * (ps["beta_h"] * ps["sigma"] * ps["gamma_u"] - ps["beta_u"] * ps["delta_h"])
            xi_2_num_2_1 = ps["gamma_h"] + ps["delta_h"]
            xi_2_num_2_2 = ps["beta_i"] * ps["sigma"] * ps["gamma_u"] - ps["delta_i"] * ps["beta_u"]
            xi_2_den_1 = pop * ps["sigma"] * ps["gamma_u"]
            xi_2_den_2 = ps["eta"] * ps["gamma_h"] + ps["gamma_i"] * (ps["gamma_h"] + ps["delta_h"])
            xi_2 = (xi_2_num_1 + (xi_2_num_2_1 * xi_2_num_2_2)) / (xi_2_den_1 * xi_2_den_2)

            xi_1_1 = 1 / ps["eta"] * (ps["beta_i"] / pop - xi_3 * ps["delta_i"])
            xi_1_2 = xi_2 * ps["gamma_i"] / ps["eta"]
            xi_1 = xi_1_1 - xi_1_2

            mu_num = (1 - ps["rho"]) * (1 - ps["sigma"])
            mu_den = ps["rho"] + ps["sigma"] - ps["rho"] * ps["sigma"]
            mu = mu_num / mu_den

            b_num = xi_2 - c_1 - c_2 * mu
            b_den = xi_3 - c_2 * mu
            b = b_num / b_den

            pop_t = pop + ps["phi"] * (i_v[e_idx[0]] + i_v[u_idx[0]]) * np.array([-1, 1])
            n_t = pop_t - (i_v[d_idx] + i_v[du_idx])

            a_s = (xi_2 - b * xi_3) / ((1 + mu) * (1 - b))
            c_c = (xi_3 - b * xi_2) / ((1 - b) * (xi_3 - c_2 * mu))
            a_d = a_s + xi_3 * c_c - xi_3
            a_h = c_c * (xi_1 - c_3) - xi_1
            a_r = c_c * (xi_2 - c_1) - xi_2
            a_ru = -c_c * c_2

            lhs = np.log(i_v[s_idx]) - np.log(fs)
            rhs = a_s * (n_t - fs) + a_d * (i_v[d_idx] + i_v[du_idx]) + \
                a_h * i_v[h_idx] + a_r * i_v[r_idx] + a_ru * i_v[ru_idx]

            return lhs-rhs

        y, _, ier, msg = fsolve(get_eqn, p.population / (2500*epi_param['beta_u']),
                                args=(p.population, epi_param, initial_values),
                                full_output=True)
        # If the solver has trouble finding the root, check how often this happens
        if ier != 1:
            print(msg)

        return np.array(y)

    def get_outbreak_peak(self, epi_param: dict, init_values: np.ndarray, patch: str) -> np.ndarray:
        """
        Calculates outbreak peaks of all patches for input parametrization of the model
        :param patch: str specifies patch for which the outbreak is calculated
        :param epi_param: dict parametrization of the model
        :param init_values: np.ndarray initial values
        :return: np.ndarray calculated outbreak peaks
        """
        # Get R0 to estimate length of time interval
        r_0 = self.get_r0(ps=epi_param)[p.patches.index(patch)]

        # Define t_end based on R_0
        if r_0 < 1.2:
            t_end = 1500
        elif r_0 < 1.5:
            t_end = 1000
        else:
            t_end = 500

        # Define time vector for the solver
        t = np.linspace(0, t_end, t_end * p.day_step)

        # Get solution
        solution = self.get_solution(t=t, epi_param=epi_param, init_values=init_values)

        # Compartments considered at outbreak
        infected = ["h"]

        # Indices of compartments
        inf_idx = get_comp_idx(comp=infected)
        outbreak_peak = np.max(solution[:, inf_idx], axis=0)

        return outbreak_peak[p.patches.index(patch)]

    @staticmethod
    def get_r0(ps: Union[dict, pd.DataFrame]) -> np.float:
        """
        Calculates R0 corresponding to the model (here it is only just a formula, not the whole procedure for
        calculating reproduction number using NGM)
        :param ps: dict, parameters of the model for which R0 is calculated
        :return: np.float, reproduction number
        """

        r0 = 0

        num = ps["rho"] * ps["beta_i"]
        den = ps["gamma_i"] + ps["delta_i"] + ps["eta"]
        r0 += num / den

        num = ps["rho"] * ps["beta_h"] * ps["eta"]
        den = (ps["gamma_i"] + ps["delta_i"] + ps["eta"]) * (ps["gamma_h"] + ps["delta_h"])
        r0 += num / den

        num = ps["beta_u"] * (1 - ps["rho"])
        den = ps["gamma_u"]
        r0 += num / den

        return r0

    def __get_model(self, xs: np.ndarray, t: np.float, ps: dict) -> np.ndarray:
        """
        Calculates derivative vector for ODE solver
        :param xs: np.ndarray, stores actual state of the system (passed by the solver)
        :param t: np.float, time point for which the solver calculates the solution
        :param ps: dict, actual parameters of the model (might change during fitting process)
        :return: np.ndarray, derivative
        """

        # NOTE, that this should be in the same order as the compartments in p.init
        s, e, i, h, u, r, ru, d, du, c = xs.reshape(-1, len(p.patches))
        if self.population_calculated_for_final_size:
            actual_population = copy.deepcopy(self.population)
        else:
            actual_population = self.population - (d + du)

        transmission = \
            ps["beta_i"] * i + \
            ps["beta_u"] * u + \
            ps["beta_h"] * h

        # NOTE, that this should be in the same order as the compartments in p.init
        model_dict = {
            # S'(t)
            "s": -s * transmission / actual_population,
            # E'(t)
            "e": s * transmission / actual_population - ps["alpha"] * e,
            # I'(t)
            "i": ps["rho"] * ps["alpha"] * e - (ps["gamma_i"] + ps["delta_i"] + ps["eta"]) * i,
            # H'(t)
            "h": ps["eta"] * i - (ps["gamma_h"] + ps["delta_h"]) * h,
            # U'(t)
            "u": (1 - ps["rho"]) * ps["alpha"] * e - ps["gamma_u"] * u,
            # R'(t)
            "r": ps["gamma_i"] * i + ps["gamma_h"] * h,
            # Ru'(t)
            "ru": (1 - ps["sigma"]) * ps["gamma_u"] * u,
            # D'(t)
            "d": ps["delta_i"] * i + ps["delta_h"] * h,
            # Du'(t)
            "du": ps["sigma"] * ps["gamma_u"] * u,
            # C'(t)
            "c": ps["rho"] * ps["alpha"] * e
        }

        model_eq = [model_dict[key] for key in p.compartments]

        return np.array(model_eq).flatten()

    def __execute_impulse(self, init: np.ndarray, param: dict):
        """
        Function executes impulsive change at lockdown
        :param init: np.ndarray, initial value model after lockdown
        :param param: dict, parameters of the model
        :return: None
        """

        # Compartments travelling at lock-down
        transport = ["e", "u"]
        # Indices of compartments
        tr_idx = np.array([p.all_states[x]
                           for x in p.all_states.keys()
                           if x.split("_")[0] in transport
                           ]).reshape((-1, p.n_patch))
        # State value at lock-down (left-hand side)
        tr = np.array([init[idx] for idx in tr_idx])
        # State value at lock-down (right-hand side
        init_mod = (tr + param["phi"] * np.array([-1, 1]) * tr[:, 0].reshape(-1, 1)).flatten()
        init[tr_idx.flatten()] = init_mod
        # Update population
        self.population += param["phi"] * np.sum(tr[:, 0]) * np.array([-1, 1])
