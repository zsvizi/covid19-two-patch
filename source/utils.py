from typing import Tuple, Union

from lmfit import Parameters
import numpy as np
import pandas as pd

from source import parameters as p


def get_parameters_from_param_dict() -> Parameters:
    """
    Creates Parameters object from user-defined parameter dictionary
    :return: Parameters
    """
    # Instantiate Parameters class
    params = Parameters()

    # Add epidemic parameters to params
    for param in p.epidemic_parameters.keys():
        add_parameter(param_dict=p.epidemic_parameters, parameter=param, params=params)

    # Add initial values to params
    if len(p.init_fitting) > 0:
        for param in p.init_fitting.keys():
            add_parameter(param_dict=p.init_fitting, parameter=param, params=params)

    for param in p.control_parameters.keys():
        if param != "T":
            add_parameter(param_dict=p.control_parameters, parameter=param, params=params)

    return params


def add_parameter(param_dict: dict, parameter: str, params: Parameters) -> None:
    """
    Add parameter to the input Parameters object from user-defined parameter dictionary
    separating cases for vary=True and vary=False
    :param param_dict: dict contains user-defined parameters (values, ranges)
    :param parameter: str name of the parameter, which is added to the Parameters object in this step
    :param params: Parameters object for storing model parameters for the fitting process
    :return: None
    """
    if str(parameter)[:-2] not in ["beta_i", "beta_h", "delta_i", "gamma_i", "eta", "delta_h", "gamma_h"]:
        params.add(name=str(parameter), value=param_dict[parameter]["value"],
                   min=param_dict[parameter]["min"], max=param_dict[parameter]["max"],
                   vary=param_dict[parameter]["vary"])
    else:
        params.add(name=str(parameter), expr=param_dict[parameter]["expr"])


def convert_to_parameter_dict(ps: Union[Parameters, dict]) -> dict:
    """
    Transform Parameters object to a dictionary or pd.DataFrame depending on the input object type
    :param ps: Union[Parameters, dict, Sampler], structure for parameters
        - Parameters object is passed during the fitting process
        - dict is passed if the model is simply solved for user-defined parameters
    :return: Union[dict, pd.DataFrame], parameter structure used by model solver
        - dict otherwise
    """
    epi_params = list(p.epidemic_parameters.keys())
    if "T" in ps.keys():
        epi_params.append("T")
    # Since model is implemented independently of patches, the parameter names shall not contain the patch ID
    # conversion returns with a dictionary containing keys without patch ID and arrays as values
    param = {key[:key.rfind("_")] if key.split("_")[-1] in p.patches else key: np.array([]) for key in epi_params}

    if isinstance(ps, Parameters):
        for key in epi_params:
            if key.split("_")[-1] in p.patches:
                param[key[:key.rfind("_")]] = np.append(param[key[:key.rfind("_")]], ps[str(key)].value)
            else:
                param[key] = np.append(param[key], ps[str(key)].value)
    elif isinstance(ps, dict):
        for key in epi_params:
            if key.split("_")[-1] in p.patches:
                param[key[:key.rfind("_")]] = np.append(param[key[:key.rfind("_")]], ps[str(key)]["value"])
            else:
                param[key] = np.append(param[key], ps[str(key)]["value"])
    return param


def get_parameter_df(sample_extended: pd.DataFrame, patch: str) -> pd.DataFrame:
    """
    Parameter getter for Sampler object (which is passed to vectorized R0 calculation)
    :param sample_extended: pd.DataFrame sample extended with not varied parameters
    :param patch: str ID of the patch for which the sensitivity analysis is done
    :return: pd.DataFrame parameter DataFrame
    """
    # Filter for specified patch
    filter_patch = [col_name
                    for col_name in sample_extended.columns
                    if col_name.split("_")[-1] == patch or col_name.split("_")[-1] not in p.patches]
    # Filtered DataFrame
    param = sample_extended[filter_patch]
    # Dictionary for renaming columns in filtered DataFrame (removing patch name from the parameter names)
    rename_dict = {key: key[:key.rfind("_")] if len(list(key.split("_"))) > 1 else key
                   for key in param.columns}

    # Filtered DataFrame with renamed columns
    param = param.rename(columns=rename_dict)
    return param


def get_description_from_parameters_dict(
        param_dict: dict, params_sa: list, patch: str = None) -> Tuple[list, dict]:
    """
    Transforms user-defined dictionary of model parameters to a description used by the Sampler object
    in sensitivity analysis, where description has the following keys:
      - names
      - num_vars
      - bounds
    :param patch:
    :param params_sa:
    :param param_dict: dict user-defined structure of parameters
    :return: Tuple[list, dict] list of varied parameter names and description used by Sampler
    """
    description = dict()

    if patch:
        names = []
        for param in params_sa:
            if param in param_dict.keys():
                names.append(param)
            elif param + '_' + patch in param_dict.keys():
                names.append(param + '_' + patch)
    else:
        names = [key for key in param_dict.keys() if key[:key.rfind("_")] in params_sa]
    description.update({"names": params_sa})
    description.update({"num_vars": len(names)})

    bounds = [[param_dict[key+'_'+patch]["s_min"], param_dict[key+'_'+patch]["s_max"]] for key in params_sa]
    description.update({"bounds": bounds})

    return names, description


def control_t(param_s: np.float, control: dict, param: str, t: np.float) -> np.float:
    """
    Returns time-dependent value for controlled parameter of the model
    :param param: str, name of the variable
    :param param_s: np.float, parameter value before control
    :param control: dict, contains parameter of the control function
    :param t: np.float, time point during model solving process
    :return: np.float, controlled parameter value
    """
    t_lock = p.control_parameters["T"]["value"]
    type_lock = p.control_parameters["type"]
    if t < t_lock:
        param_t = param_s
    else:
        if type_lock == 'step':
            param_t = control[str(param) + "_target"]["value"]
        elif type_lock == 'exponential':
            param_t = control[str(param) + "_target"]["value"] + \
                      (param_s - control[str(param) + "_target"]["value"]) * \
                      np.exp(-control[str(param) + "_q"]["value"] * (t - t_lock))
        else:
            raise Exception("Invalid type was given!")
    return param_t


def get_comp_idx(comp: Union[str, list]) -> list:
    """
    Get indices from p.all_states for the input compartment or list of compartments
    :param comp: Union[str, list] compartment or list of compartments
    :return: list indices corresponding to input compartments
    """
    if isinstance(comp, str):
        return [p.all_states[x] for x in p.all_states.keys() if x.split("_")[0] == comp]
    elif isinstance(comp, list):
        return [p.all_states[x] for x in p.all_states.keys() if x.split("_")[0] in comp]
