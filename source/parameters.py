"""
UTILITIES
"""

# Initial population vector for the patches
population = [0, 0]

# List of compartments
compartments = ["s", "e", "i", "h", "u", "r", "ru", "d", "du", "c"]
c_dict = {key: idx for idx, key in enumerate(list(compartments))}

# Notation for the patches
patches = ['A', 'B']
p_dict = {key: idx for idx, key in enumerate(list(patches))}
n_patch = len(patches)

# Get all state combinations
all_states = dict()
for idx_c, state in enumerate(compartments):
    for idx_p, patch in enumerate(patches):
        all_states.update({str(state) + "_" + str(patch): n_patch * idx_c + idx_p})

# Dictionary for translating compartment name to column name in data
# nuovi_positivi is the incidence (new cases from one day to another) and not i from the model
comp_to_data_it = {'h_A': ('totale_ospedalizzati', 'A'),
                   'h_B': ('totale_ospedalizzati', 'B'),
                   'd_A': ('deceduti', 'A'),
                   'd_B': ('deceduti', 'B'),
                   'c_A': ('nuovi_positivi', 'A'),
                   'c_B': ('nuovi_positivi', 'B')
                   }

comp_to_data_chn = {'h_A': ('current_severe', 'A'),
                    'h_B': ('current_severe', 'B'),
                    'd_A': ('total_deaths', 'A'),
                    'd_B': ('total_deaths', 'B'),
                    'c_A': ('daily_inf', 'A'),
                    'c_B': ('daily_inf', 'B')
                    }

# Initialize dates from data
dates = dict()

# Division of one day in model solver
day_step = 1

# Filters
c_idx = [all_states[x] for x in all_states.keys() if x.split("_")[0] == "c"]

d_idx = [
    [all_states[x]
     for x in all_states.keys()
     if x.split("_")[0] in ["d", "du"] and x.split("_")[1] == patch
     ]
    for patch in patches
]

"""
EXPERIMENTAL DATA
"""
# Initial values
init_main = {"s": [0., 0],
             "e": [9517, 486],
             "i": [94, 0],
             "h": [125, 2],
             "u": [554, 5],
             "r": [0, 1],
             "ru": [0, 0],
             "d": [7, 0],
             "du": [0, 0],
             "c": [219, 2]}
init = {key: init_main[key] for key in compartments}

# Initial values chosen for fitting
init_fitting = {}

# data for fitting (used for dummy data creation)
# add c_X for daily incidence in patch X
fitting = ['h_A', 'd_A', 'c_A', 'h_B', 'd_B', 'c_B']

# Epidemic parameters with values, interval (min, max) for fitting and
# switch ("vary") for including parameter estimation
# To ignore a parameter in SA analysis, set its s_min and s_max to equal.
epidemic_parameters_main = {
    "beta_u": {"value": 2, "min": 0.01, "max": 6, "vary": False, "s_min": 0.1, "s_max": 1.25},
    "gamma_u": {"value": 1 / 7, "min": 0.05, "max": 0.2, "vary": False},
    "alpha": {"value": 1 / 5.5, "min": 0.001, "max": 1, "vary": False},
    "sigma": {"value": 0.001, "min": 0, "max": 1, "vary": False},
    "f_i": {"value": 0.25, "min": 0.15, "max": 0.8, "vary": False},
    "f_h": {"value": 0.10, "min": 0.01, "max": 0.15, "vary": False}
}

# This dictionary contains keys with patch IDs
epidemic_parameters = dict()
for patch in patches:
    for key, value in zip(epidemic_parameters_main.keys(), epidemic_parameters_main.values()):
        epidemic_parameters.update({key + "_" + patch: value})

epidemic_parameters.update(
    {"phi": {"value": 0.1, "min": 0, "max": 1, "vary": False},
     # Probability of getting hospitalized after 2 days
     "i_H_A": {"value": 0.75, "min": 0, "max": 1, "vary": False},
     "i_H_B": {"value": 0.50, "min": 0, "max": 1, "vary": False},
     "i_R_A": {"value": 0.99, "min": 0.60, "max": 1, "vary": False},
     "i_R_B": {"value": 0.99, "min": 0.80, "max": 1, "vary": False},
     "delta_i_A": {"expr": "(1-i_H_A)*(1-i_R_A)/10", "s_min": 1e-4, "s_max": 1e-3},
     "delta_i_B": {"expr": "(1-i_H_B)*(1-i_R_B)/10", "s_min": 1e-4, "s_max": 1e-3},
     "gamma_i_A": {"expr": "(1-i_H_A)*i_R_A/10", "s_min": 0.009, "s_max": 0.09},
     "gamma_i_B": {"expr": "(1-i_H_B)*i_R_B/10", "s_min": 0.009, "s_max": 0.09},
     "eta_A": {"expr": "i_H_A/2", "s_min": 0.1, "s_max": 0.5},
     "eta_B": {"expr": "i_H_B/2", "s_min": 0.1, "s_max": 0.5},
     # Probability of recovery from the hospital after 10 days
     "h_R_A": {"value": 0.85, "min": 0, "max": 1, "vary": False},
     "h_R_B": {"value": 0.90, "min": 0, "max": 1, "vary": False},
     "delta_h_A": {"expr": "(1-h_R_A)/10", "s_min": 5e-3, "s_max": 5e-2},
     "delta_h_B": {"expr": "(1-h_R_B)/10", "s_min": 5e-3, "s_max": 5e-2},
     "gamma_h_A": {"expr": "h_R_A/10", "s_min": 0.01, "s_max": 0.1},
     "gamma_h_B": {"expr": "h_R_B/10", "s_min": 0.01, "s_max": 0.1},
     # Detection rate
     "rho_A": {"value": 0.145, "min": 0.05, "max": 0.9, "vary": False, "s_min": 0.1, "s_max": 0.9},
     "rho_B": {"value": 0.175, "min": 0.05, "max": 0.9, "vary": False, "s_min": 0.1, "s_max": 0.9},
     "beta_i_A": {"expr": "f_i_A*beta_u_A"},
     "beta_h_A": {"expr": "f_h_A*beta_u_A"},
     "beta_i_B": {"expr": "f_i_B*beta_u_B"},
     "beta_h_B": {"expr": "f_h_B*beta_u_B"}
     }
)

control_parameters = {
    "T": {"value": 0, "min": 0.001, "max": 1, "vary": False, "s_min": 1, "s_max": 100}
}
