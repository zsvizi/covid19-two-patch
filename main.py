import copy
import datetime
import itertools
import os

from lmfit import report_fit, conf_interval
from lmfit.printfuncs import report_ci
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
import numpy as np

import source
from source import parameters as p
from source.dataloader import Dataloader, generate_chinese_data, get_data, init_from_data
from source.model import TwoPatchModel
from source.optimizer import Optimizer, setup_parameters
from source.parameter_handler import ParameterHandler
from source.sensitivity.analyzer import Analyzer
from source.sensitivity.sampler import Sampler
from source.sensitivity.target_generator import TargetGenerator
from source.utils import convert_to_parameter_dict, get_description_from_parameters_dict, get_parameters_from_param_dict
np.random.seed(0)
plt.rc('xtick', labelsize=20)
plt.rc('ytick', labelsize=20)
plt.rc('font', size=20)


def main():
    # --------------------------------------------------------------------------
    # INITIALIZATION

    # Get available data for Italy
    dl_it = Dataloader(country="Italy")

    # Get available data for Chine
    generate_chinese_data()
    dl_chn = Dataloader(country="China")

    # Initialize based on data
    init_from_data(data_loader=dl_it)

    # Initial values
    init_values = np.array(list(p.init.values())).flatten()

    # Dictionary for referring to a specified state
    idx = {comp: list(range(p.n_patch * idx, p.n_patch * (idx + 1)))
           for comp, idx in zip(p.c_dict.keys(), p.c_dict.values())}

    # Select plotting
    model_plot = False
    does_optimizer_run = True
    plot_model_solution_ci = False
    sensitivity = False
    analytical_final_size = False

    # --------------------------------------------------------------------------
    # MODEL

    # print('Answer all yes/no questions with 1 (for yes) or 0 (for no)')
    # model_plot = int(input('Plot the model with default parameters? '))
    if model_plot:
        # bool for selecting final size plotting
        is_final_size_plotted = True
        # value for T can be define via date: p.dates["2020-03-09"]
        p.control_parameters["T"]["value"] = p.dates["2020-03-09"]

        # Time vector
        start_day = p.dates["2020-02-24"]
        end_day = p.dates["2020-07-15"]

        # Instantiate model
        model = TwoPatchModel(final_size=True)

        # Instantiate ParameterHandler
        param_hand = ParameterHandler()
        param_hand.load()
        # param_hand.add_time_interval(time_point=["2020-02-24", "2020-03-09"])
        # param_hand.add_time_interval(time_point=["2020-03-09", "2020-05-04"])
        # param_hand.add_time_interval(time_point=["2020-05-04", "2020-06-15"])
        # param_hand.add_parameters(parameters=convert_to_parameter_dict(params))
        # param_hand.add_parameters(parameters=convert_to_parameter_dict(params))
        # param_hand.add_parameters(parameters=convert_to_parameter_dict(params))
        # param_hand.add_init_values(params_obj=params)
        # param_hand.add_init_values(params_obj=params)
        # param_hand.add_init_values(params_obj=params)

        # Update initial values based on ParameterHandler
        init = copy.deepcopy(init_values)
        param_hand.update_initial_values(initial_values=init)

        base = 1
        mod = 2 if base == 1 else 1
        if is_final_size_plotted:
            param_hand.parameters[mod] = param_hand.parameters[base]
            p.dates.update({'2020-12-31': 300})
            param_hand.time_intervals[2][1] = '2020-12-31'
            end_day = p.dates["2020-12-31"]

        params = param_hand.parameters[base]
        param_hand.parameters[1].update({'phi': param_hand.phi})
        # Get model solution for fitted parameters
        time_vector, model_solution = model.get_piecewise_parametrized_solution(param_hand=param_hand,
                                                                                init_values=init)

        # Specify compartments for plotting
        time_filter = slice(start_day * p.day_step, end_day * p.day_step)
        if is_final_size_plotted:
            model_sol = np.zeros((len(time_vector[time_filter]), 2))
            final_size_comps = [["r_A", "r_B"], ["ru_A", "ru_B"], ["d_A", "d_B"], ["du_A", "du_B"]]
            for cmps in final_size_comps:
                comps_to_plot = cmps
                comps = np.array([idx[comp.split('_')[0]][p.p_dict[comp.split('_')[1]]]
                                  for comp in comps_to_plot]).flatten()

                model_sol += model_solution[time_filter, comps]

            # Get initial values for final size calculation
            p.control_parameters["T"]["value"] = p.dates[param_hand.time_intervals[1][0]]
            init_val = model_solution[p.control_parameters["T"]["value"] * p.day_step, :]
            # Calculate analytical final size
            epi_p = param_hand.parameters[2]
            epi_p.update({"phi": param_hand.phi})
            final_size_values = model.get_analytical_final_size(epi_param=epi_p,
                                                                initial_values=init_val)
            fs = np.ones(len(time_vector[time_filter])).reshape((-1, 1)) * \
                 (final_size_values)
            # Plot the solution the specified solutions
            fig = plt.figure(figsize=(10, 6))
            times_before_migration = slice(start_day * p.day_step, p.control_parameters["T"]["value"] * p.day_step)
            times_after_migration = slice(p.control_parameters["T"]["value"] * p.day_step, end_day * p.day_step)
            plt.plot(time_vector[times_before_migration], p.population[0] - model_sol[times_before_migration, 0], 'r',
                     p.population[1] - model_sol[times_before_migration, 1], 'b')
            plt.plot(time_vector[times_after_migration], model.population[0] - model_sol[times_after_migration, 0], 'r',
                     model.population[1] - model_sol[times_after_migration, 1], 'b')
            plt.plot(time_vector[time_filter], fs[:, 0], 'r--', fs[:, 1], 'b--')
            plt.legend([r'$S_{\infty}$ (region A)', r'$S_{\infty}$ (region B)'])
            plt.show()
            fig.savefig(os.path.join(source.PATH, 'data/generated', 'final_size_' + str(base) + '.eps'), format='eps')
        else:
            # Specify compartments for plotting
            comps_to_plot = ["i_A", "h_A", "c_A"]
            comps = np.array([idx[comp.split('_')[0]][p.p_dict[comp.split('_')[1]]]
                              for comp in comps_to_plot]).flatten()

            # Plot the solution the specified solutions
            time_filter = slice(start_day * p.day_step, 10 * p.day_step)
            plt.plot(time_vector[time_filter], model_solution[time_filter, comps])
            plt.legend(comps_to_plot)
            plt.show()

        print(model.get_r0(ps=params))

    # --------------------------------------------------------------------------
    # OPTIMIZATION
    if does_optimizer_run:
        # -- User-defined parameters --
        # Define time intervals
        time_points = ["2020-02-24", "2020-03-09", "2020-03-22", "2020-05-04"]  # , "2020-06-15", "2020-07-16"]
        # value for lock-down (T) can be define via date: p.dates["2020-03-09"]
        p.control_parameters["T"]["value"] = p.dates["2020-03-09"]

        # PLEASE CHECK, THAT ALL PARAMETERS BELOW ARE ADDED TO THE CORRESPONDING DICTIONARY IN parameters.py!
        # These dictionaries are: init_fitting, epidemic_parameters_main, control_parameters_main
        # p.init_fitting.update({'c_A': {"value": 900, "min": 1, "max": 100000, "vary": False}})

        # Define parameters to vary in different intervals
        list_for_vary = [
            ["beta_u_A", "beta_u_B"],
            ["beta_u_A", "beta_u_B", "i_H_A", "i_H_B", "h_R_A", "h_R_B"],
            ["beta_u_A", "beta_u_B", "i_H_A", "i_H_B", "h_R_A", "h_R_B"],
            # ["i_H_A", "i_H_B", "h_R_A", "h_R_B"],
            # ["i_H_A", "i_H_B", "h_R_A", "h_R_B"],
        ]

        # Define control parameters to vary in different intervals
        list_for_vary_control = [
            [], [], []  # , [], []
        ]

        # Define initial values to vary in different intervals
        list_for_vary_init = [
            [], [], []  # , [], []
        ]

        # -- Initialization --
        # model
        model = TwoPatchModel()
        # optimizer objects will be created for all optimization steps and stored in this variable
        optimizer = None
        # Initial values for the current optimization step
        init_values_opt = copy.deepcopy(init_values)
        # Incidence init values
        last_day_cumulative = None
        # Instantiate ParameterHandler object
        param_hand = ParameterHandler()

        # -- Piecewise optimization --
        # Loop through all time intervals
        for t_iv in range(len(time_points)-1):

            # --- Preparing parameters for current optimization step ---
            params = setup_parameters(list_for_vary=list_for_vary,
                                      list_for_vary_control=list_for_vary_control,
                                      list_for_vary_init=list_for_vary_init,
                                      optimizer=optimizer,
                                      t_iv=t_iv)

            # --- Initialize data for fitting ---
            # Define start and end day for the next optimization step
            start_day = time_points[t_iv]
            end_day = time_points[t_iv+1]
            print('-------- {} - {} --------'.format(start_day, end_day))

            # Get data for fitting
            data_fit, data_fit_time = get_data(data_loader=dl_it,
                                               start_day=start_day,
                                               end_day=end_day,
                                               columns=p.fitting)

            # --- Execute optimization and get model solution completed up to this step ---
            # Instantiate Optimizer class and fit parameters
            optimizer = Optimizer(time_vector=data_fit_time, data=data_fit, init_values=init_values_opt, params=params,
                                  last_day_cumulative=last_day_cumulative, model=model)

            # Update parameter handler
            param_hand.add_time_interval(time_point=[start_day, end_day])
            param_hand.add_parameters(parameters=convert_to_parameter_dict(ps=optimizer.result.params))
            param_hand.add_init_values(params_obj=optimizer.result.params)
            if "phi" in list_for_vary[t_iv]:
                param_hand.phi = optimizer.result.params["phi"].value

            # Get model solution for fitted parameters
            init = copy.deepcopy(init_values)
            param_hand.update_initial_values(initial_values=init)
            time_vector, model_solution = \
                optimizer.model.get_piecewise_parametrized_solution(param_hand=param_hand,
                                                                    init_values=init)
            model = copy.deepcopy(optimizer.model)

            # Store cumulative values from last day for the next optimization step
            last_day_cumulative = optimizer.model.last_day_cumulative

            # Store state at last time point for initializing next optimization step
            init_values_opt = model_solution[-1, :]

            # Create figure
            fig, _ = plt.subplots(2, 3, figsize=(15, 10))
            for i, patch in enumerate(p.patches):
                # hospitalized and deaths
                filter_idx = [p.all_states[state] for state in sorted(p.fitting) if patch in state and
                              'c' not in state]
                fig.axes[3*i].plot(time_vector, model_solution[:, np.array(filter_idx)], 'x')
                fig.axes[3*i].legend(np.array(list(p.all_states.keys()))[filter_idx])
                fig.axes[3*i].set_prop_cycle(None)
                filter_idx = [idx for idx, state in enumerate(sorted(p.fitting)) if patch in state and
                              'c' not in state]
                fig.axes[3*i].plot(np.arange(p.dates[start_day], p.dates[end_day]),
                                   optimizer.data[:, np.array(filter_idx)], 'o')
                # daily incidence/cumulative cases
                filter_idx = [p.all_states[state] for state in sorted(p.fitting) if patch in state and 'c' in state]
                fig.axes[3*i+1].plot(time_vector, model_solution[:, np.array(filter_idx)], 'x')
                fig.axes[3*i+1].legend(np.array(list(p.all_states.keys()))[filter_idx])
                fig.axes[3*i+1].set_prop_cycle(None)
                filter_idx = [idx for idx, state in enumerate(sorted(p.fitting))
                              if patch in state and 'c' in state]
                fig.axes[3*i+1].plot(np.arange(p.dates[start_day], p.dates[end_day]),
                                     optimizer.data[:, np.array(filter_idx)], 'o')
                # exposed, infected and unreported
                filter_idx = [p.all_states[state] for state in p.all_states.keys()
                              if patch in state and ('i' in state or state[0] == 'u' or 'e' in state)]
                fig.axes[3*i+2].plot(time_vector, model_solution[:, np.array(filter_idx)], 'x')
                fig.axes[3*i+2].legend(np.array(list(p.all_states.keys()))[filter_idx])
            # Show all plots in one figure
            plt.show()

            # --- Get more details about fitting result ---
            # Get R0 for defined parameters
            print("R0 for fitted model", optimizer.model.get_r0(ps=param_hand.parameters[-1]))

            # Print report about fitting result to the console
            report_fit(optimizer.result)

            # Print confidence intervals
            # ci = conf_interval(optimizer.minimizer, optimizer.result)
            # report_ci(ci)

        # Save initial value of phi if it was not considered in the optimization
        if param_hand.phi is None:
            param_hand.phi = p.epidemic_parameters["phi"]["value"]
        # Save stored parametrization
        param_hand.save()

        # Save the model solutions
        np.save(file=str(os.path.join(source.PATH, "data/generated/model_solution.npy")), arr=model_solution)

        # Get data for all the time intervals combined
        # Cumulative cases are plotted here instead of daily incidences
        data_fit, data_fit_time = get_data(data_loader=dl_it,
                                           start_day=time_points[0],
                                           end_day=time_points[-1],
                                           columns=p.fitting)

        # Create a figure with all time intervals combined
        fig, _ = plt.subplots(1, 3, figsize=(20, 7))
        state_names = ['Cumulative Cases', 'Deaths', 'Currently Hospitalized']
        start_date = datetime.datetime(year=2020, month=2, day=24)
        date_list = [start_date + datetime.timedelta(days=x) for x in time_vector]
        for i, state in enumerate(['c', 'd', 'h']):
            colors = itertools.cycle(["red", "blue"])
            for patch in p.patches:
                color = next(colors)
                fit_idx = p.all_states[state+'_'+patch]
                data_idx = [k for k, comp in enumerate(sorted(p.fitting)) if comp == state+'_'+patch][0]
                output = np.cumsum(model_solution[:, fit_idx]) if state == 'c' else model_solution[:, fit_idx]
                target = np.cumsum(data_fit[:, data_idx]) if state == 'c' else data_fit[:, data_idx]
                fig.axes[i].xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))
                fig.axes[i].plot_date(date_list, output, marker='x', color=color)
                fig.axes[i].plot_date(date_list[:-1], target, marker='o', color=color)
                fig.axes[i].axvline(datetime.datetime(year=2020, month=3, day=9), linestyle='--', color='k')
                fig.axes[i].axvline(datetime.datetime(year=2020, month=3, day=22), linestyle='--', color='k')
            fig.autofmt_xdate(rotation=45)
            fig.tight_layout()
            fig.axes[i].set_title(state_names[i])
        # Show all plots in one figure
        plt.show()
        fig.savefig(os.path.join(source.PATH, 'data/generated', 'ITfit_before_and_during_lockdown.eps'), format='eps')

        # Plot how the parameters change with time
        plot_params = ['beta_u', 'eta', 'rho', 'delta_h', 'gamma_h']
        fig, _ = plt.subplots(len(plot_params), 1)
        for i, param in enumerate(plot_params):
            ps_A_t = np.array([ps[param][0] for ps in param_hand.parameters])
            ps_B_t = np.array([ps[param][1] for ps in param_hand.parameters])
            num_intervals = len(list_for_vary)
            fig.axes[i].step(np.arange(num_intervals), ps_A_t, 'b')
            fig.axes[i].step(np.arange(num_intervals), ps_B_t, 'r')
            fig.axes[i].legend(['A', 'B'])
            fig.axes[i].set_title(param)
        plt.show()

    if plot_model_solution_ci:
        # Define time of lock-down
        p.control_parameters["T"]["value"] = p.dates["2020-03-09"]

        # --- Initialization
        # Choose the parameters to calculate and plot PRCC
        params_sa = ['beta_u']

        # Get parameters for sensitivity analysis
        parameters_all = dict()
        parameters_all.update(p.epidemic_parameters)
        parameters_all.update({"T": p.control_parameters["T"]})

        # Create and fill ParameterHandler
        param_hand = ParameterHandler()
        param_hand.load()
        # Update initial values from ParameterHandler
        init = copy.deepcopy(init_values)
        param_hand.update_initial_values(initial_values=init)

        # --- Sampler
        # Get description for Sampler object from collected parameters
        varied_parameters, description = get_description_from_parameters_dict(param_dict=parameters_all,
                                                                              params_sa=params_sa)

        # Vary the parameters by 5% from the best fit
        vary_by = 0.10
        bounds = []
        for idx, name in enumerate(param_hand.parameters[0].keys()):
            if name in params_sa:
                ps = list(param_hand.parameters[0][name])
                bounds.extend([[patch - vary_by * patch, patch + vary_by * patch] for patch in ps])
        description["bounds"] = bounds

        # Generate samples
        number_of_samples = 1000
        sampler = Sampler(description=description,
                          method="LHS",
                          number_of_samples=number_of_samples)
        x = sampler.output

        # --- TargetGenerator
        # Generate target variables
        target_generator = TargetGenerator(sampler=sampler,
                                           param_hand=param_hand,
                                           varied_parameters=varied_parameters)

        time_sim, model_outputs = target_generator.generate_model_solutions(init_values=init)
        time_sim = time_sim.astype(int)

        start = p.dates[param_hand.time_intervals[0][0]]
        end = p.dates[param_hand.time_intervals[0][1]]
        time_fit = np.linspace(start, end, int(end - start) * p.day_step + 1)[:-1]
        time_fit = time_fit.astype(int)

        model_solution = np.load(file=str(os.path.join(source.PATH, "data/generated/model_solution.npy")))
        model_solution = model_solution[time_fit, :]

        # Load data used for fitting
        data, _ = get_data(data_loader=dl_it,
                           start_day=param_hand.time_intervals[0][0],
                           end_day=param_hand.time_intervals[0][1],
                           columns=p.fitting)

        # Plot the figure
        fig, _ = plt.subplots(2, 2, figsize=(15, 10))
        for i, patch in enumerate(p.patches):
            colors = itertools.cycle(["red", "green", "blue"])
            for comp in ['h', 'd']:
                color = next(colors)
                filter_idx = [p.all_states[state] for state in sorted(p.fitting) if patch in state and comp in state]
                # for k in range(number_of_samples):
                #    fig.axes[2*i+j].plot(time_vector, model_outputs[k][:, np.array(filter_idx)], 'y',
                #                         alpha=0.005)
                band_max = np.maximum.reduce(model_outputs)[:, np.array(filter_idx)].squeeze()
                band_min = np.minimum.reduce(model_outputs)[:, np.array(filter_idx)].squeeze()
                fig.axes[2*i].fill_between(time_sim, band_max, band_min, color=color, alpha=0.25)

                fig.axes[2*i].plot(time_fit, model_solution[:, np.array(filter_idx)], color=color)
                filter_idx = [idx for idx, state in enumerate(sorted(p.fitting)) if patch in state and comp in state]
                fig.axes[2*i].plot(time_fit, data[:, np.array(filter_idx)], marker='o', color=color)
                fig.axes[2*i].legend(['h', 'd'])
                fig.axes[2*i].set_title("Patch {}".format(patch))
            color = next(colors)
            filter_idx = [p.all_states[state] for state in sorted(p.fitting) if patch in state and 'c' in state]
            # for k in range(number_of_samples):
            #    fig.axes[2*i+j].plot(time_vector, model_outputs[k][:, np.array(filter_idx)], 'y',
            #                         alpha=0.005)
            band_max = np.maximum.reduce(model_outputs)[:, np.array(filter_idx)].squeeze()
            band_min = np.minimum.reduce(model_outputs)[:, np.array(filter_idx)].squeeze()
            fig.axes[2*i+1].fill_between(time_sim, band_max, band_min, color=color, alpha=0.25)

            fig.axes[2*i+1].plot(time_fit, model_solution[:, np.array(filter_idx)], color=color)
            filter_idx = [idx for idx, state in enumerate(sorted(p.fitting)) if patch in state and 'c' in state]
            fig.axes[2*i+1].plot(time_fit, data[:, np.array(filter_idx)], marker='o', color=color)
            fig.axes[2*i+1].legend(['c'])
            fig.axes[2*i+1].set_title("Patch {}".format(patch))
        # Show all plots in one figure
        plt.show()

    # --------------------------------------------------------------------------
    # SENSITIVITY ANALYSIS
    # Generate description dictionary used in sampler (for sensitivity)
    # sensitivity = int(input('Run sensitivity analysis? '))
    if sensitivity:
        # Define time of lock-down
        p.control_parameters["T"]["value"] = p.dates["2020-03-09"]

        # --- Initialization
        # Specify patch for sensitivity analysis
        patch = input('Select a patch (A, B): ')

        # Choose the parameters to calculate and plot sensitivity indices
        params_sa = ['beta_u', 'rho', 'eta', 'gamma_i', 'gamma_h', 'delta_i', 'delta_h']

        # Get parameters for sensitivity analysis
        parameters_all = dict()
        parameters_all.update(p.epidemic_parameters)
        parameters_all.update({"T": p.control_parameters["T"]})

        # --- Sampler
        # Get description for Sampler object from collected parameters
        varied_parameters, description = get_description_from_parameters_dict(param_dict=parameters_all,
                                                                              params_sa=params_sa,
                                                                              patch=patch)

        # Generate samples
        method = input('Select a method (prcc, sobol, morris): ')
        if method == 'prcc':
            sampling_method = 'LHS'
        elif method == 'sobol':
            sampling_method = 'saltelli'
        elif method == 'morris':
            sampling_method = 'morris'
        else:
            raise Exception("Unknown method")
        sampler = Sampler(description=description,
                          method=sampling_method,
                          number_of_samples=50000)
        x = sampler.output

        # --- TargetGenerator
        # Create and fill ParameterHandler
        param_hand = ParameterHandler()
        param_hand.load()
        # Update initial values from ParameterHandler
        init = copy.deepcopy(init_values)
        param_hand.update_initial_values(initial_values=init)

        # Generate target variables
        target_generator = TargetGenerator(sampler=sampler,
                                           param_hand=param_hand,
                                           varied_parameters=varied_parameters)

        # Selection of target values: ['R0', 'outbreak_peak', 'final_size']
        target = input('Select the target value (R0, outbreak_peak, final_size): ')
        if target == 'R0':
            y = target_generator.generate_r0_values(patch=patch)

        elif target == 'outbreak_peak':
            y = target_generator.generate_outbreak_peak_values(init_values=init,
                                                               patch=patch)
        elif target == 'final_size':
            y = target_generator.generate_final_size_values(init_values=init,
                                                            patch=patch,
                                                            param_hand=param_hand)
        else:
            raise Exception("Unknown target value")

        # --- Analyzer
        # Analyze sensitivity on Sampler output
        sa = Analyzer(input_table=x,
                      target=y,
                      variables=params_sa,
                      method=method)

        # Print output
        print("{} method".format(method))
        print(sa.output)

        # Plot sensitivity indices
        sa.plot_sensitivity_indices(file_name=method + '_' + target + '_' + patch)

    # --------------------------------------------------------------------------
    # ANALYTICAL FINAL SIZE

    if analytical_final_size:
        # Set date of lockdown
        p.control_parameters["T"]["value"] = p.dates["2020-03-09"]

        # Create and initialize ParameterHandler
        param_hand = ParameterHandler()
        param_hand.load()
        # Update initial values based on ParameterHandler
        param_hand.update_initial_values(initial_values=init_values)

        # Instantiate model and get piecewise solution based on ParameterHandler
        model = TwoPatchModel()
        _, solution = model.get_piecewise_parametrized_solution(param_hand=param_hand,
                                                                init_values=init_values)
        # Get initial values for final size calculation
        init = solution[p.control_parameters["T"]["value"] * p.day_step, :]
        # Calculate analytical final size
        final_size_values = model.get_analytical_final_size(epi_param=param_hand.parameters[1],
                                                            initial_values=init)
        print(final_size_values)


if __name__ == "__main__":
    main()
